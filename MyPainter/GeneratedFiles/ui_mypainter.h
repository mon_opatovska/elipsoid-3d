/********************************************************************************
** Form generated from reading UI file 'mypainter.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MYPAINTER_H
#define UI_MYPAINTER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MyPainterClass
{
public:
    QAction *actionOpen;
    QAction *actionSave;
    QAction *actionClear;
    QAction *actionNew;
    QAction *actionNegative;
    QAction *actionSlow_Negative;
    QAction *actionMedium_Negative;
    QAction *actionSalt_Pepper;
    QAction *actionMedian_filter;
    QAction *actionRotate_left;
    QAction *actionRotate_right;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QGridLayout *gridLayout_2;
    QPushButton *pushButton;
    QSpacerItem *verticalSpacer;
    QGroupBox *groupBox_2;
    QWidget *formLayoutWidget_2;
    QFormLayout *formLayout_2;
    QLabel *label_6;
    QLabel *label_7;
    QLabel *label_8;
    QSpinBox *spinBox_4;
    QSpinBox *spinBox_5;
    QSpinBox *spinBox_6;
    QGroupBox *groupBox_3;
    QWidget *formLayoutWidget_3;
    QFormLayout *formLayout_3;
    QLabel *label_9;
    QLabel *label_10;
    QLabel *label_11;
    QSpinBox *spinBox_7;
    QSpinBox *spinBox_8;
    QSpinBox *spinBox_9;
    QRadioButton *radioButton_4;
    QPushButton *pushButton_2;
    QCheckBox *checkBox_2;
    QCheckBox *checkBox;
    QRadioButton *radioButton_3;
    QTabWidget *tabWidget;
    QWidget *tab;
    QFormLayout *formLayout_4;
    QLabel *label;
    QSpinBox *spinBox;
    QLabel *label_2;
    QSpinBox *spinBox_2;
    QLabel *label_3;
    QSpinBox *spinBox_3;
    QWidget *tab_2;
    QLabel *label_16;
    QLabel *label_17;
    QLineEdit *lineEdit;
    QLineEdit *lineEdit_2;
    QPushButton *pushButton_3;
    QComboBox *comboBox;
    QWidget *tab_3;
    QLabel *label_18;
    QSpinBox *spinBox_11;
    QLabel *label_19;
    QSpinBox *spinBox_12;
    QPushButton *pushButton_4;
    QRadioButton *radioButton;
    QTabWidget *tabWidget_2;
    QWidget *tab_4;
    QLabel *label_13;
    QDoubleSpinBox *doubleSpinBox;
    QLabel *label_14;
    QDoubleSpinBox *doubleSpinBox_2;
    QLabel *label_15;
    QDoubleSpinBox *doubleSpinBox_3;
    QWidget *tab_5;
    QLabel *label_20;
    QDoubleSpinBox *doubleSpinBox_4;
    QLabel *label_21;
    QLabel *label_22;
    QDoubleSpinBox *doubleSpinBox_5;
    QDoubleSpinBox *doubleSpinBox_6;
    QWidget *tab_6;
    QLabel *label_23;
    QLabel *label_24;
    QLabel *label_25;
    QDoubleSpinBox *doubleSpinBox_7;
    QDoubleSpinBox *doubleSpinBox_8;
    QDoubleSpinBox *doubleSpinBox_9;
    QWidget *tab_7;
    QSpinBox *spinBox_10;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents_3;
    QGroupBox *groupBox;
    QWidget *formLayoutWidget;
    QFormLayout *formLayout;
    QLabel *label_4;
    QSlider *horizontalSlider;
    QLabel *label_5;
    QSlider *horizontalSlider_2;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuEffects;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MyPainterClass)
    {
        if (MyPainterClass->objectName().isEmpty())
            MyPainterClass->setObjectName(QStringLiteral("MyPainterClass"));
        MyPainterClass->resize(824, 712);
        MyPainterClass->setStyleSheet(QStringLiteral("background-color: rgb(255, 170, 255);"));
        actionOpen = new QAction(MyPainterClass);
        actionOpen->setObjectName(QStringLiteral("actionOpen"));
        actionSave = new QAction(MyPainterClass);
        actionSave->setObjectName(QStringLiteral("actionSave"));
        actionClear = new QAction(MyPainterClass);
        actionClear->setObjectName(QStringLiteral("actionClear"));
        actionNew = new QAction(MyPainterClass);
        actionNew->setObjectName(QStringLiteral("actionNew"));
        actionNegative = new QAction(MyPainterClass);
        actionNegative->setObjectName(QStringLiteral("actionNegative"));
        actionSlow_Negative = new QAction(MyPainterClass);
        actionSlow_Negative->setObjectName(QStringLiteral("actionSlow_Negative"));
        actionMedium_Negative = new QAction(MyPainterClass);
        actionMedium_Negative->setObjectName(QStringLiteral("actionMedium_Negative"));
        actionSalt_Pepper = new QAction(MyPainterClass);
        actionSalt_Pepper->setObjectName(QStringLiteral("actionSalt_Pepper"));
        actionMedian_filter = new QAction(MyPainterClass);
        actionMedian_filter->setObjectName(QStringLiteral("actionMedian_filter"));
        actionRotate_left = new QAction(MyPainterClass);
        actionRotate_left->setObjectName(QStringLiteral("actionRotate_left"));
        actionRotate_right = new QAction(MyPainterClass);
        actionRotate_right->setObjectName(QStringLiteral("actionRotate_right"));
        centralWidget = new QWidget(MyPainterClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, -1, -1, -1);
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setSpacing(6);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, -1, 40, -1);
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        gridLayout_2->addWidget(pushButton, 7, 0, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_2->addItem(verticalSpacer, 11, 0, 1, 1);

        groupBox_2 = new QGroupBox(centralWidget);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setMinimumSize(QSize(0, 120));
        formLayoutWidget_2 = new QWidget(groupBox_2);
        formLayoutWidget_2->setObjectName(QStringLiteral("formLayoutWidget_2"));
        formLayoutWidget_2->setGeometry(QRect(10, 20, 160, 95));
        formLayout_2 = new QFormLayout(formLayoutWidget_2);
        formLayout_2->setSpacing(6);
        formLayout_2->setContentsMargins(11, 11, 11, 11);
        formLayout_2->setObjectName(QStringLiteral("formLayout_2"));
        formLayout_2->setContentsMargins(0, 0, 0, 0);
        label_6 = new QLabel(formLayoutWidget_2);
        label_6->setObjectName(QStringLiteral("label_6"));

        formLayout_2->setWidget(0, QFormLayout::LabelRole, label_6);

        label_7 = new QLabel(formLayoutWidget_2);
        label_7->setObjectName(QStringLiteral("label_7"));

        formLayout_2->setWidget(1, QFormLayout::LabelRole, label_7);

        label_8 = new QLabel(formLayoutWidget_2);
        label_8->setObjectName(QStringLiteral("label_8"));

        formLayout_2->setWidget(2, QFormLayout::LabelRole, label_8);

        spinBox_4 = new QSpinBox(formLayoutWidget_2);
        spinBox_4->setObjectName(QStringLiteral("spinBox_4"));
        spinBox_4->setMaximum(255);
        spinBox_4->setValue(200);

        formLayout_2->setWidget(0, QFormLayout::FieldRole, spinBox_4);

        spinBox_5 = new QSpinBox(formLayoutWidget_2);
        spinBox_5->setObjectName(QStringLiteral("spinBox_5"));
        spinBox_5->setMaximum(255);
        spinBox_5->setValue(100);

        formLayout_2->setWidget(1, QFormLayout::FieldRole, spinBox_5);

        spinBox_6 = new QSpinBox(formLayoutWidget_2);
        spinBox_6->setObjectName(QStringLiteral("spinBox_6"));
        spinBox_6->setMaximum(255);
        spinBox_6->setValue(100);

        formLayout_2->setWidget(2, QFormLayout::FieldRole, spinBox_6);


        gridLayout_2->addWidget(groupBox_2, 8, 0, 1, 1);

        groupBox_3 = new QGroupBox(centralWidget);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        groupBox_3->setMinimumSize(QSize(175, 0));
        groupBox_3->setMaximumSize(QSize(16777215, 16777215));
        formLayoutWidget_3 = new QWidget(groupBox_3);
        formLayoutWidget_3->setObjectName(QStringLiteral("formLayoutWidget_3"));
        formLayoutWidget_3->setGeometry(QRect(10, 20, 160, 95));
        formLayout_3 = new QFormLayout(formLayoutWidget_3);
        formLayout_3->setSpacing(6);
        formLayout_3->setContentsMargins(11, 11, 11, 11);
        formLayout_3->setObjectName(QStringLiteral("formLayout_3"));
        formLayout_3->setContentsMargins(0, 0, 0, 0);
        label_9 = new QLabel(formLayoutWidget_3);
        label_9->setObjectName(QStringLiteral("label_9"));

        formLayout_3->setWidget(0, QFormLayout::LabelRole, label_9);

        label_10 = new QLabel(formLayoutWidget_3);
        label_10->setObjectName(QStringLiteral("label_10"));

        formLayout_3->setWidget(1, QFormLayout::LabelRole, label_10);

        label_11 = new QLabel(formLayoutWidget_3);
        label_11->setObjectName(QStringLiteral("label_11"));

        formLayout_3->setWidget(2, QFormLayout::LabelRole, label_11);

        spinBox_7 = new QSpinBox(formLayoutWidget_3);
        spinBox_7->setObjectName(QStringLiteral("spinBox_7"));
        spinBox_7->setMaximum(255);
        spinBox_7->setValue(255);

        formLayout_3->setWidget(0, QFormLayout::FieldRole, spinBox_7);

        spinBox_8 = new QSpinBox(formLayoutWidget_3);
        spinBox_8->setObjectName(QStringLiteral("spinBox_8"));
        spinBox_8->setMaximum(255);
        spinBox_8->setValue(100);

        formLayout_3->setWidget(1, QFormLayout::FieldRole, spinBox_8);

        spinBox_9 = new QSpinBox(formLayoutWidget_3);
        spinBox_9->setObjectName(QStringLiteral("spinBox_9"));
        spinBox_9->setMaximum(255);
        spinBox_9->setValue(255);

        formLayout_3->setWidget(2, QFormLayout::FieldRole, spinBox_9);


        gridLayout_2->addWidget(groupBox_3, 8, 1, 1, 1);

        radioButton_4 = new QRadioButton(centralWidget);
        radioButton_4->setObjectName(QStringLiteral("radioButton_4"));

        gridLayout_2->addWidget(radioButton_4, 5, 1, 1, 1);

        pushButton_2 = new QPushButton(centralWidget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));

        gridLayout_2->addWidget(pushButton_2, 7, 1, 1, 1);

        checkBox_2 = new QCheckBox(centralWidget);
        checkBox_2->setObjectName(QStringLiteral("checkBox_2"));

        gridLayout_2->addWidget(checkBox_2, 6, 1, 1, 1);

        checkBox = new QCheckBox(centralWidget);
        checkBox->setObjectName(QStringLiteral("checkBox"));

        gridLayout_2->addWidget(checkBox, 6, 0, 1, 1);

        radioButton_3 = new QRadioButton(centralWidget);
        radioButton_3->setObjectName(QStringLiteral("radioButton_3"));

        gridLayout_2->addWidget(radioButton_3, 5, 0, 1, 1);

        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabWidget->setMinimumSize(QSize(0, 150));
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        formLayout_4 = new QFormLayout(tab);
        formLayout_4->setSpacing(6);
        formLayout_4->setContentsMargins(11, 11, 11, 11);
        formLayout_4->setObjectName(QStringLiteral("formLayout_4"));
        label = new QLabel(tab);
        label->setObjectName(QStringLiteral("label"));

        formLayout_4->setWidget(0, QFormLayout::LabelRole, label);

        spinBox = new QSpinBox(tab);
        spinBox->setObjectName(QStringLiteral("spinBox"));

        formLayout_4->setWidget(0, QFormLayout::FieldRole, spinBox);

        label_2 = new QLabel(tab);
        label_2->setObjectName(QStringLiteral("label_2"));

        formLayout_4->setWidget(1, QFormLayout::LabelRole, label_2);

        spinBox_2 = new QSpinBox(tab);
        spinBox_2->setObjectName(QStringLiteral("spinBox_2"));

        formLayout_4->setWidget(1, QFormLayout::FieldRole, spinBox_2);

        label_3 = new QLabel(tab);
        label_3->setObjectName(QStringLiteral("label_3"));

        formLayout_4->setWidget(2, QFormLayout::LabelRole, label_3);

        spinBox_3 = new QSpinBox(tab);
        spinBox_3->setObjectName(QStringLiteral("spinBox_3"));
        spinBox_3->setMaximum(500);

        formLayout_4->setWidget(2, QFormLayout::FieldRole, spinBox_3);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        label_16 = new QLabel(tab_2);
        label_16->setObjectName(QStringLiteral("label_16"));
        label_16->setGeometry(QRect(10, 10, 151, 19));
        label_17 = new QLabel(tab_2);
        label_17->setObjectName(QStringLiteral("label_17"));
        label_17->setGeometry(QRect(10, 40, 141, 19));
        lineEdit = new QLineEdit(tab_2);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(160, 10, 113, 25));
        lineEdit_2 = new QLineEdit(tab_2);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));
        lineEdit_2->setGeometry(QRect(160, 40, 113, 25));
        pushButton_3 = new QPushButton(tab_2);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));
        pushButton_3->setGeometry(QRect(160, 70, 112, 34));
        comboBox = new QComboBox(tab_2);
        comboBox->setObjectName(QStringLiteral("comboBox"));
        comboBox->setGeometry(QRect(0, 70, 131, 25));
        tabWidget->addTab(tab_2, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QStringLiteral("tab_3"));
        label_18 = new QLabel(tab_3);
        label_18->setObjectName(QStringLiteral("label_18"));
        label_18->setGeometry(QRect(10, 10, 101, 16));
        spinBox_11 = new QSpinBox(tab_3);
        spinBox_11->setObjectName(QStringLiteral("spinBox_11"));
        spinBox_11->setGeometry(QRect(120, 10, 42, 22));
        label_19 = new QLabel(tab_3);
        label_19->setObjectName(QStringLiteral("label_19"));
        label_19->setGeometry(QRect(10, 40, 101, 16));
        spinBox_12 = new QSpinBox(tab_3);
        spinBox_12->setObjectName(QStringLiteral("spinBox_12"));
        spinBox_12->setGeometry(QRect(120, 40, 42, 22));
        pushButton_4 = new QPushButton(tab_3);
        pushButton_4->setObjectName(QStringLiteral("pushButton_4"));
        pushButton_4->setGeometry(QRect(10, 80, 75, 23));
        radioButton = new QRadioButton(tab_3);
        radioButton->setObjectName(QStringLiteral("radioButton"));
        radioButton->setGeometry(QRect(100, 80, 82, 17));
        tabWidget->addTab(tab_3, QString());

        gridLayout_2->addWidget(tabWidget, 4, 0, 1, 1);

        tabWidget_2 = new QTabWidget(centralWidget);
        tabWidget_2->setObjectName(QStringLiteral("tabWidget_2"));
        tabWidget_2->setMinimumSize(QSize(0, 110));
        tab_4 = new QWidget();
        tab_4->setObjectName(QStringLiteral("tab_4"));
        label_13 = new QLabel(tab_4);
        label_13->setObjectName(QStringLiteral("label_13"));
        label_13->setGeometry(QRect(6, 0, 41, 20));
        doubleSpinBox = new QDoubleSpinBox(tab_4);
        doubleSpinBox->setObjectName(QStringLiteral("doubleSpinBox"));
        doubleSpinBox->setGeometry(QRect(20, 0, 62, 22));
        doubleSpinBox->setMaximum(1);
        doubleSpinBox->setSingleStep(0.1);
        doubleSpinBox->setValue(0.9);
        label_14 = new QLabel(tab_4);
        label_14->setObjectName(QStringLiteral("label_14"));
        label_14->setGeometry(QRect(5, 30, 16, 20));
        doubleSpinBox_2 = new QDoubleSpinBox(tab_4);
        doubleSpinBox_2->setObjectName(QStringLiteral("doubleSpinBox_2"));
        doubleSpinBox_2->setGeometry(QRect(20, 30, 62, 22));
        doubleSpinBox_2->setMaximum(1);
        doubleSpinBox_2->setSingleStep(0.1);
        doubleSpinBox_2->setValue(0.8);
        label_15 = new QLabel(tab_4);
        label_15->setObjectName(QStringLiteral("label_15"));
        label_15->setGeometry(QRect(6, 60, 16, 20));
        doubleSpinBox_3 = new QDoubleSpinBox(tab_4);
        doubleSpinBox_3->setObjectName(QStringLiteral("doubleSpinBox_3"));
        doubleSpinBox_3->setGeometry(QRect(20, 60, 62, 22));
        doubleSpinBox_3->setMaximum(1);
        doubleSpinBox_3->setSingleStep(0.1);
        doubleSpinBox_3->setValue(0.7);
        tabWidget_2->addTab(tab_4, QString());
        tab_5 = new QWidget();
        tab_5->setObjectName(QStringLiteral("tab_5"));
        label_20 = new QLabel(tab_5);
        label_20->setObjectName(QStringLiteral("label_20"));
        label_20->setGeometry(QRect(10, 0, 41, 20));
        doubleSpinBox_4 = new QDoubleSpinBox(tab_5);
        doubleSpinBox_4->setObjectName(QStringLiteral("doubleSpinBox_4"));
        doubleSpinBox_4->setGeometry(QRect(30, 0, 62, 22));
        doubleSpinBox_4->setMaximum(1);
        doubleSpinBox_4->setSingleStep(0.1);
        doubleSpinBox_4->setValue(0.7);
        label_21 = new QLabel(tab_5);
        label_21->setObjectName(QStringLiteral("label_21"));
        label_21->setGeometry(QRect(10, 30, 16, 20));
        label_22 = new QLabel(tab_5);
        label_22->setObjectName(QStringLiteral("label_22"));
        label_22->setGeometry(QRect(10, 60, 16, 20));
        doubleSpinBox_5 = new QDoubleSpinBox(tab_5);
        doubleSpinBox_5->setObjectName(QStringLiteral("doubleSpinBox_5"));
        doubleSpinBox_5->setGeometry(QRect(30, 30, 62, 22));
        doubleSpinBox_5->setMaximum(1);
        doubleSpinBox_5->setSingleStep(0.1);
        doubleSpinBox_5->setValue(0.5);
        doubleSpinBox_6 = new QDoubleSpinBox(tab_5);
        doubleSpinBox_6->setObjectName(QStringLiteral("doubleSpinBox_6"));
        doubleSpinBox_6->setGeometry(QRect(30, 60, 62, 22));
        doubleSpinBox_6->setMaximum(1);
        doubleSpinBox_6->setSingleStep(0.1);
        doubleSpinBox_6->setValue(0.6);
        tabWidget_2->addTab(tab_5, QString());
        tab_6 = new QWidget();
        tab_6->setObjectName(QStringLiteral("tab_6"));
        label_23 = new QLabel(tab_6);
        label_23->setObjectName(QStringLiteral("label_23"));
        label_23->setGeometry(QRect(10, 0, 41, 20));
        label_24 = new QLabel(tab_6);
        label_24->setObjectName(QStringLiteral("label_24"));
        label_24->setGeometry(QRect(10, 30, 16, 20));
        label_25 = new QLabel(tab_6);
        label_25->setObjectName(QStringLiteral("label_25"));
        label_25->setGeometry(QRect(10, 60, 16, 20));
        doubleSpinBox_7 = new QDoubleSpinBox(tab_6);
        doubleSpinBox_7->setObjectName(QStringLiteral("doubleSpinBox_7"));
        doubleSpinBox_7->setGeometry(QRect(30, 0, 62, 22));
        doubleSpinBox_7->setMaximum(1);
        doubleSpinBox_7->setSingleStep(0.1);
        doubleSpinBox_7->setValue(0.4);
        doubleSpinBox_8 = new QDoubleSpinBox(tab_6);
        doubleSpinBox_8->setObjectName(QStringLiteral("doubleSpinBox_8"));
        doubleSpinBox_8->setGeometry(QRect(30, 30, 62, 22));
        doubleSpinBox_8->setMaximum(1);
        doubleSpinBox_8->setSingleStep(0.1);
        doubleSpinBox_8->setValue(0.8);
        doubleSpinBox_9 = new QDoubleSpinBox(tab_6);
        doubleSpinBox_9->setObjectName(QStringLiteral("doubleSpinBox_9"));
        doubleSpinBox_9->setGeometry(QRect(30, 60, 62, 22));
        doubleSpinBox_9->setMaximum(1);
        doubleSpinBox_9->setSingleStep(0.1);
        doubleSpinBox_9->setValue(0.7);
        tabWidget_2->addTab(tab_6, QString());
        tab_7 = new QWidget();
        tab_7->setObjectName(QStringLiteral("tab_7"));
        spinBox_10 = new QSpinBox(tab_7);
        spinBox_10->setObjectName(QStringLiteral("spinBox_10"));
        spinBox_10->setGeometry(QRect(20, 10, 111, 22));
        spinBox_10->setMinimum(1);
        spinBox_10->setMaximum(10000000);
        spinBox_10->setValue(15);
        tabWidget_2->addTab(tab_7, QString());

        gridLayout_2->addWidget(tabWidget_2, 10, 0, 1, 1);


        horizontalLayout->addLayout(gridLayout_2);

        scrollArea = new QScrollArea(centralWidget);
        scrollArea->setObjectName(QStringLiteral("scrollArea"));
        scrollArea->setStyleSheet(QStringLiteral("background-color: rgb(255, 170, 255);"));
        scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents_3 = new QWidget();
        scrollAreaWidgetContents_3->setObjectName(QStringLiteral("scrollAreaWidgetContents_3"));
        scrollAreaWidgetContents_3->setGeometry(QRect(0, 0, 368, 543));
        scrollArea->setWidget(scrollAreaWidgetContents_3);

        horizontalLayout->addWidget(scrollArea);


        verticalLayout->addLayout(horizontalLayout);

        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setMinimumSize(QSize(0, 100));
        formLayoutWidget = new QWidget(groupBox);
        formLayoutWidget->setObjectName(QStringLiteral("formLayoutWidget"));
        formLayoutWidget->setGeometry(QRect(0, 20, 1061, 80));
        formLayout = new QFormLayout(formLayoutWidget);
        formLayout->setSpacing(6);
        formLayout->setContentsMargins(11, 11, 11, 11);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        formLayout->setContentsMargins(0, 0, 0, 0);
        label_4 = new QLabel(formLayoutWidget);
        label_4->setObjectName(QStringLiteral("label_4"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label_4);

        horizontalSlider = new QSlider(formLayoutWidget);
        horizontalSlider->setObjectName(QStringLiteral("horizontalSlider"));
        horizontalSlider->setMaximum(180);
        horizontalSlider->setOrientation(Qt::Horizontal);

        formLayout->setWidget(0, QFormLayout::FieldRole, horizontalSlider);

        label_5 = new QLabel(formLayoutWidget);
        label_5->setObjectName(QStringLiteral("label_5"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_5);

        horizontalSlider_2 = new QSlider(formLayoutWidget);
        horizontalSlider_2->setObjectName(QStringLiteral("horizontalSlider_2"));
        horizontalSlider_2->setMaximum(360);
        horizontalSlider_2->setOrientation(Qt::Horizontal);

        formLayout->setWidget(1, QFormLayout::FieldRole, horizontalSlider_2);


        verticalLayout->addWidget(groupBox);

        MyPainterClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MyPainterClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 824, 21));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        menuEffects = new QMenu(menuBar);
        menuEffects->setObjectName(QStringLiteral("menuEffects"));
        MyPainterClass->setMenuBar(menuBar);
        statusBar = new QStatusBar(MyPainterClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MyPainterClass->setStatusBar(statusBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuEffects->menuAction());
        menuFile->addAction(actionNew);
        menuFile->addAction(actionOpen);
        menuFile->addAction(actionSave);
        menuEffects->addAction(actionClear);

        retranslateUi(MyPainterClass);
        QObject::connect(actionOpen, SIGNAL(triggered()), MyPainterClass, SLOT(ActionOpen()));
        QObject::connect(actionSave, SIGNAL(triggered()), MyPainterClass, SLOT(ActionSave()));
        QObject::connect(actionClear, SIGNAL(triggered()), MyPainterClass, SLOT(EffectClear()));
        QObject::connect(actionNew, SIGNAL(triggered()), MyPainterClass, SLOT(ActionNew()));
        QObject::connect(horizontalSlider, SIGNAL(valueChanged(int)), MyPainterClass, SLOT(Rotacia()));
        QObject::connect(horizontalSlider_2, SIGNAL(valueChanged(int)), MyPainterClass, SLOT(Rotacia()));
        QObject::connect(pushButton, SIGNAL(clicked()), MyPainterClass, SLOT(Generuj()));
        QObject::connect(pushButton_2, SIGNAL(clicked()), MyPainterClass, SLOT(Generuj2()));
        QObject::connect(pushButton_3, SIGNAL(clicked()), MyPainterClass, SLOT(Vykresli()));
        QObject::connect(pushButton_4, SIGNAL(clicked()), MyPainterClass, SLOT(Plocha()));

        tabWidget->setCurrentIndex(1);
        tabWidget_2->setCurrentIndex(3);


        QMetaObject::connectSlotsByName(MyPainterClass);
    } // setupUi

    void retranslateUi(QMainWindow *MyPainterClass)
    {
        MyPainterClass->setWindowTitle(QApplication::translate("MyPainterClass", "MyPainter", 0));
        actionOpen->setText(QApplication::translate("MyPainterClass", "Open", 0));
        actionSave->setText(QApplication::translate("MyPainterClass", "Save", 0));
        actionClear->setText(QApplication::translate("MyPainterClass", "Clear", 0));
        actionNew->setText(QApplication::translate("MyPainterClass", "New", 0));
        actionNegative->setText(QApplication::translate("MyPainterClass", "Negative", 0));
        actionSlow_Negative->setText(QApplication::translate("MyPainterClass", "Black-White", 0));
        actionMedium_Negative->setText(QApplication::translate("MyPainterClass", "Sepia", 0));
        actionSalt_Pepper->setText(QApplication::translate("MyPainterClass", "Salt-Pepper", 0));
        actionMedian_filter->setText(QApplication::translate("MyPainterClass", "Median filter", 0));
        actionRotate_left->setText(QApplication::translate("MyPainterClass", "Rotate left", 0));
        actionRotate_right->setText(QApplication::translate("MyPainterClass", "Rotate right", 0));
        pushButton->setText(QApplication::translate("MyPainterClass", "Generuj", 0));
        groupBox_2->setTitle(QApplication::translate("MyPainterClass", "Farba svetla zo zdroja", 0));
        label_6->setText(QApplication::translate("MyPainterClass", "R", 0));
        label_7->setText(QApplication::translate("MyPainterClass", "G", 0));
        label_8->setText(QApplication::translate("MyPainterClass", "B", 0));
        groupBox_3->setTitle(QApplication::translate("MyPainterClass", "Farba okoliteho svetla", 0));
        label_9->setText(QApplication::translate("MyPainterClass", "R", 0));
        label_10->setText(QApplication::translate("MyPainterClass", "G", 0));
        label_11->setText(QApplication::translate("MyPainterClass", "B", 0));
        radioButton_4->setText(QApplication::translate("MyPainterClass", "Stredove", 0));
        pushButton_2->setText(QApplication::translate("MyPainterClass", "Generuj2(cez file open)", 0));
        checkBox_2->setText(QApplication::translate("MyPainterClass", "Gouraudovo tienovanie", 0));
        checkBox->setText(QApplication::translate("MyPainterClass", "Konstantne tienovanie", 0));
        radioButton_3->setText(QApplication::translate("MyPainterClass", "Rovnobezne", 0));
        label->setText(QApplication::translate("MyPainterClass", "Pocet poludnikov", 0));
        label_2->setText(QApplication::translate("MyPainterClass", "Pocet rovnobeziek", 0));
        label_3->setText(QApplication::translate("MyPainterClass", "Vzdialenost", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("MyPainterClass", "elipsoid", 0));
        label_16->setText(QApplication::translate("MyPainterClass", "Koeficient skalovania", 0));
        label_17->setText(QApplication::translate("MyPainterClass", "Uhol otocenia(Pi/..)", 0));
        pushButton_3->setText(QApplication::translate("MyPainterClass", "Vykresli", 0));
        comboBox->clear();
        comboBox->insertItems(0, QStringList()
         << QApplication::translate("MyPainterClass", "Skalovanie", 0)
         << QApplication::translate("MyPainterClass", "Otocenie dolava", 0)
         << QApplication::translate("MyPainterClass", "Otocenie doprava", 0)
        );
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("MyPainterClass", "transformacie", 0));
        label_18->setText(QApplication::translate("MyPainterClass", "pocet deleni krivky", 0));
        label_19->setText(QApplication::translate("MyPainterClass", "pocet kriviek plochy", 0));
        pushButton_4->setText(QApplication::translate("MyPainterClass", "OK", 0));
        radioButton->setText(QApplication::translate("MyPainterClass", "Rotacia", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_3), QApplication::translate("MyPainterClass", "plocha", 0));
        label_13->setText(QApplication::translate("MyPainterClass", "R", 0));
        label_14->setText(QApplication::translate("MyPainterClass", "G", 0));
        label_15->setText(QApplication::translate("MyPainterClass", "B", 0));
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_4), QApplication::translate("MyPainterClass", "koef. odrazu", 0));
        label_20->setText(QApplication::translate("MyPainterClass", "R", 0));
        label_21->setText(QApplication::translate("MyPainterClass", "G", 0));
        label_22->setText(QApplication::translate("MyPainterClass", "B", 0));
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_5), QApplication::translate("MyPainterClass", "koef. difuzie", 0));
        label_23->setText(QApplication::translate("MyPainterClass", "R", 0));
        label_24->setText(QApplication::translate("MyPainterClass", "G", 0));
        label_25->setText(QApplication::translate("MyPainterClass", "B", 0));
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_6), QApplication::translate("MyPainterClass", "ambientny koef.", 0));
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_7), QApplication::translate("MyPainterClass", "ostrost zrkad. koef.", 0));
        groupBox->setTitle(QApplication::translate("MyPainterClass", "Otacanie", 0));
        label_4->setText(QApplication::translate("MyPainterClass", "Zenit", 0));
        label_5->setText(QApplication::translate("MyPainterClass", "Azimut", 0));
        menuFile->setTitle(QApplication::translate("MyPainterClass", "File", 0));
        menuEffects->setTitle(QApplication::translate("MyPainterClass", "Effects", 0));
    } // retranslateUi

};

namespace Ui {
    class MyPainterClass: public Ui_MyPainterClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MYPAINTER_H
