#ifndef MYPAINTER_H
#define MYPAINTER_H

#include <QtWidgets/QMainWindow>
#include "ui_mypainter.h"
#include "paintwidget.h"
#include <QElapsedTimer>
#include <qspinbox.h>
#include <qradiobutton.h>

class MyPainter : public QMainWindow
{
	Q_OBJECT

public:
	MyPainter(QWidget *parent = 0);
	~MyPainter();


public slots:
	void ActionOpen();
	void ActionSave();
	void EffectClear();
	void ActionNew();
	void Generuj();
	void Generuj2();
	void Rotacia();
	void Vykresli();
	void Plocha();
private:
	Ui::MyPainterClass ui;
	PaintWidget paintWidget;
};

#endif // MYPAINTER_H
