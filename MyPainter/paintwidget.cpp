#include "paintwidget.h"
using namespace std;

PaintWidget::PaintWidget(QWidget *parent)
	: QWidget(parent)
{
	setAttribute(Qt::WA_StaticContents);
	modified = false;
	painting = false;
	myPenWidth = 1;
	myPenColor = Qt::blue;
}

bool PaintWidget::openImage(const QString &fileName)
{
	body.clear();
	indexy.clear();
	QFile loaded(fileName);
	loaded.open(QIODevice::ReadOnly);

	QString riadok;
	QStringList slova;
	QPoint P;
	while (!riadok.contains("POINTS")) riadok = loaded.readLine();

	int pocet;
	pocet = riadok.split(" ")[1].toInt();
	printf("pocet %d",pocet);
	for (int i = 0; i < pocet; i++) {
		riadok = loaded.readLine();
		slova = riadok.split(" ");
		P.setX(slova[0].toDouble());
		P.setY(slova[1].toDouble());
		body.push_back(P);
		surZ.push_back(slova[2].toDouble());
	}
	printf("otvoreny");
	while (!riadok.contains("POLYGONS")) riadok = loaded.readLine();
	pocet_poly = riadok.split(" ")[1].toInt();

	for (int i = 0; i < pocet_poly; i++) {
		riadok = loaded.readLine();
		slova = riadok.split(" ");
		indexy.push_back(slova[1].toInt());
		indexy.push_back(slova[2].toInt());
		indexy.push_back(slova[3].toInt());
	}
	loaded.close();

	return true;
}

bool PaintWidget::newImage(int x, int y)
{
	QImage loadedImage(x,y,QImage::Format_RGB32);
	loadedImage.fill(qRgb(255, 255, 255));
	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

bool PaintWidget::saveImage(const QString &fileName)
{
	QImage visibleImage = image;
	resizeImage(&visibleImage, size());

	if (visibleImage.save(fileName,"png")) {
		modified = false;
		return true;
	}
	else {
		return false;
	}
}

void PaintWidget::setPenColor(const QColor &newColor)
{
	myPenColor = newColor;
}

void PaintWidget::setPenWidth(int newWidth)
{
	myPenWidth = newWidth;
}

void PaintWidget::clearImage()
{
	image.fill(qRgb(255, 255, 255));
	modified = true;
	update();
}

void PaintWidget::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton) {
		lastPoint = event->pos();
		painting = true;
		QPainter painter(&image);
		painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
		painter.drawPoint(lastPoint);
		BodyPlocha.push_back(QVector3D(lastPoint.x(),lastPoint.y(),0));
		update();
	}
}

void PaintWidget::mouseDoubleClickEvent(QMouseEvent *event)
{

}

void PaintWidget::mouseMoveEvent(QMouseEvent *event)
{
	if ((event->buttons() & Qt::RightButton) && painting)
		drawLineTo(event->pos());

	
}

void PaintWidget::mouseReleaseEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton) {
		lastPoint = event->pos();
		painting = true;
	}

	if (event->button() == Qt::RightButton && painting) {
		drawLineTo(event->pos());
		painting = true;
		rightclick = true;
	}

	if (rightclick == true) {
		posunutie();
		update();
	}
	rightclick = false;
}

void PaintWidget::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	QRect dirtyRect = event->rect();
	painter.drawImage(dirtyRect, image, dirtyRect);
}

void PaintWidget::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);
}

void PaintWidget::drawLineTo(const QPoint &endPoint)
{
	QPainter painter(&image);
	//	QColor C(200, 255, 255);
	//	painter.setPen(QPen(C, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	//	painter.drawLine(lastPoint2, endPoint);
	modified = true;

	int rad = (myPenWidth / 2) + 2;
	update(QRect(lastPoint2, endPoint).normalized().adjusted(-rad, -rad, +rad, +rad));
	lastPoint2 = endPoint;
	posunutie_body.push_back(lastPoint2);
}

void PaintWidget::resizeImage(QImage *image, const QSize &newSize)
{
	if (image->size() == newSize)
		return;

	QImage newImage(newSize, QImage::Format_RGB32);
	newImage.fill(qRgb(255, 255, 255));
	QPainter painter(&newImage);
	painter.drawImage(QPoint(0, 0), *image);
	*image = newImage;
}

int PaintWidget::selectKth(int * data, int s, int e, int k)
{
	// 5 or less elements: do a small insertion sort
	if (e - s <= 5)
	{
		for (int i = s + 1; i < e; i++)
			for (int j = i; j > 0 && data[j - 1] > data[j]; j--) std::swap(data[j], data[j - 1]);
		return s + k;
	}

	int p = (s + e) / 2; // choose simply center element as pivot

						 // partition around pivot into smaller and larger elements
	std::swap(data[p], data[e - 1]); // temporarily move pivot to the end
	int j = s;  // new pivot location to be calculated
	for (int i = s; i + 1 < e; i++)
		if (data[i] < data[e - 1]) std::swap(data[i], data[j++]);
	std::swap(data[j], data[e - 1]);

	// recurse into the applicable partition
	if (k == j - s) return s + k;
	else if (k < j - s) return selectKth(data, s, j, k);
	else return selectKth(data, j + 1, e, k - j + s - 1); // subtract amount of smaller elements from k
}

void PaintWidget::generuj(int rovnobezky, int poludniky) //n-rovnobezka, m-poludnik
{
	body.clear();
	indexy.clear();
	QPoint A;
	double alfa = 0, beta = 0;
	int a = 10, b = 15, c = 20; //polosi
	int xs = image.width();
	int ys = image.height();
	rovnobezky++;

	fstream file;
	file.open("elipsoid2.vtk", fstream::out);

	file << "# vtk DataFile Version 3.0" << endl;
	file << "vtk output" << endl << "ASCII" << endl << "DATASET POLYDATA" << endl;

	double poma = 2 * M_PI / poludniky;
	double pomb = M_PI / rovnobezky;
	
	SUR tmp; //struktura so suradnicami x,y,z

	file << "POINTS " << poludniky*(rovnobezky + 1) << " float" << endl;

	for (int i = 0; i <= rovnobezky; i++)
	{
		alfa = 0;
		for (int j = 0; j < poludniky; j++)
		{
			tmp.x = (xs/2)+(a*cos(alfa)*sin(beta))*10.0;
			tmp.y = (ys/2)+(b*sin(alfa)*sin(beta))*10.0;
			tmp.z = (c*cos(beta))*10.0;

			A.setX(tmp.x);
			A.setY(tmp.y);
			body.push_back(A);
			surZ.push_back(tmp.z);

			alfa += poma;
		}
		beta += pomb;
	}

	for (int i = 0; i < poludniky*(rovnobezky + 1); i++)
	{
		file << body[i].x() << " " << body[i].y() << " " << surZ[i] << endl;
	}

	pocet_poly = ((rovnobezky*poludniky) - poludniky) * 2;
	file << "POLYGONS " << pocet_poly << " " << 4 * pocet_poly << endl;
	
	rovnobezky--;

	for (int i = poludniky; i < poludniky * 2; i++)
	{
		if (i == (poludniky * 2) - 1)
		{
			file << "3 " << poludniky << " ";
			indexy.push_back(poludniky);
		}
			
		else
		{
			file << "3 " << i + 1 << " ";
			indexy.push_back(i + 1);
		}
			
		file << i << " 0" << endl;
		indexy.push_back(i);
		indexy.push_back(0);

	}

	 
	for (int i = poludniky; i < (pocet_poly / 2); i++)
	{
		if (i%poludniky == poludniky - 1)
		{
			i -= poludniky;
			file << "3 " << i + poludniky << " " << i + 1 << " " << i + 2 * poludniky << endl;
			file << "3 " << i + 1 << " " << i + poludniky + 1 << " " << i + 2 * poludniky << endl;
			indexy.push_back(i + poludniky);
			indexy.push_back(i + 1);
			indexy.push_back(i + 2*poludniky);
			indexy.push_back(i + 1);
			indexy.push_back(i + poludniky+1);
			indexy.push_back(i + 2*poludniky);
			i += poludniky;
		}
		else
		{
			file << "3 " << i << " " << i + 1 << " " << i + poludniky << endl;
			file << "3 " << i + 1 << " " << i + poludniky + 1 << " " << i + poludniky << endl;
			indexy.push_back(i);
			indexy.push_back(i + 1);
			indexy.push_back(i + poludniky);
			indexy.push_back(i + 1);
			indexy.push_back(i + poludniky+1);
			indexy.push_back(i + poludniky);
		}

	}

	for (int i = pocet_poly / 2; i < poludniky*(rovnobezky + 1); i++)
	{

		if (i == (poludniky*(rovnobezky + 1)) - 1)
		{
			file << "3 " << i << " " << pocet_poly / 2 << " " << poludniky*(rovnobezky + 1) << endl;
			indexy.push_back(i);
			indexy.push_back(pocet_poly / 2);
			indexy.push_back(poludniky*(rovnobezky + 1));
		}
		else
		{
			file << "3 " << i << " " << i + 1 << " " << poludniky*(rovnobezky + 1) << endl;
			indexy.push_back(i);
			indexy.push_back(i + 1);
			indexy.push_back(poludniky*(rovnobezky + 1));
		}
	}
	
	Zpom = surZ;
	Zpom1 = surZ;

	file.close();
}

void PaintWidget::rotuj(double PHI, double THETA, bool priemet)
{
	double phi = M_PI*PHI / 180.0;
	double theta = M_PI*THETA / 180.0;
	int xs = image.width();
	int ys = image.height();
	
	if (priemet == 0) //rotacia rovnobezneho priemetu
	{
		for (int i = 0; i < Bodiky.size(); i++) 
		{
			int xx, yy;
			int it = indexy[i];
			xx = body[it].x() - (xs/2);
			yy = body[it].y() - (ys / 2);
			
			Bodiky[i].setX((xx*cos(phi) - yy*sin(phi)) + (xs / 2));
			Bodiky[i].setY((xx*sin(phi)*cos(theta) + yy*cos(phi)*cos(theta) - surZ[it] * sin(theta)) + (ys / 2));
			Zpom[i] = ((xx*sin(phi)*sin(theta)) + (xs / 2)) + ((yy*cos(theta)*sin(phi)) + (ys / 2)) + surZ[it] * cos(theta);
		}
	}

	/*if (priemet == 1)//rotacia stredoveho priemetu
	{
		for (int i = 0; i < Bodiky.size(); i++)
		{
			int xx, yy;
			int it = indexy[i];
			xx = ((((body[it].x() - (xs / 2)) * dz) / (dz - surZ[it])) + (xs / 2)) - (xs / 2);
			yy = ((((body[it].y() - (ys / 2)) * dz) / (dz - surZ[it])) + (ys / 2)) - (ys / 2);


			Bodiky[i].setX((xx*cos(phi) - yy*sin(phi)) + (xs / 2));
			Bodiky[i].setY((xx*sin(phi)*cos(theta) + yy*cos(phi)*cos(theta) - Zpom1[it]*sin(theta)) + (ys / 2));
			Zpom[i] = ((xx*sin(phi)*sin(theta)) + (xs / 2)) + ((yy*cos(theta)*sin(phi)) + (ys / 2)) + Zpom1[it] * cos(theta);
		}
	}*/
}

bool PaintWidget::kontrola_bodu(QVector3D bod) {
	
	if (bod.x() < 0 || bod.x() >= image.width()) 
		return true;
	

	if (bod.y() < 0 || bod.y() >= image.height()) 
		return true;
	
	//ak je ni mimo -> false
	return false;
}

QVector3D PaintWidget::norma(QVector3D vektor) 
{
	QVector3D normovany_vektor;

	//normovanie
	float a = sqrt( vektor.x() * vektor.x() + vektor.y() * vektor.y() + vektor.z() * vektor.z());
	
	normovany_vektor.setX(vektor.x() / a);
	normovany_vektor.setY(vektor.y() / a);
	normovany_vektor.setZ(vektor.z() / a);

	return normovany_vektor;
}

int PaintWidget::kontrola_farby(float zlozka)
{
	int farba;

	if (zlozka < 0.0)
		farba = 0;

	if (zlozka > 255.0)
		farba = 255;

	if ((zlozka >= 0.0) && (zlozka <= 255))
		farba = int(zlozka);

	return farba;
}

QVector<QColor>PaintWidget::Phongov_osv_model()
{
	QVector3D normala, svetelny_luc, odrazeny_luc, vektor_pohladu;

	QVector<QColor> farba, IAmbientna, IDifuzna, IZrkadlova;

	float w1, w2, w3;
	int i = 0;

	IAmbientna.resize(pocet_poly);
	IDifuzna.resize(pocet_poly);
	IZrkadlova.resize(pocet_poly);
	farba.resize(pocet_poly);
	
	for (int j = 0; j < pocet_poly; j++)//normala v kazdej ploske
	{
		//vypocet normaly N
		//u2*v3 - u3*v2
		w1 = ((Bodiky[i + 1].y() - Bodiky[i].y())*(surZ[i + 2] - surZ[i + 1])) - ((surZ[i + 1] - surZ[i])*(Bodiky[i + 2].y() - Bodiky[i + 1].y()));
		//u3*v1 - u1*v3
		w2 = ((surZ[i + 1] - surZ[i])*(Bodiky[i + 2].x() - Bodiky[i + 1].x())) - ((Bodiky[i + 1].x() - Bodiky[i].x())*(surZ[i + 2] - surZ[i + 1]));
		//u1*v2 - u2*v1
		w3 = ((Bodiky[i + 1].x() - Bodiky[i].x())*(Bodiky[i + 2].y() - Bodiky[i + 1].y())) - ((Bodiky[i + 1].y() - Bodiky[i].y())*(Bodiky[i + 2].x() - Bodiky[i + 1].x()));
		i = i + 3;
		
		normala.setX(w1);
		normala.setY(w2);
		normala.setZ(w3);
		normala = norma(normala); //znormovana normala

		//printf("normala %.2f %.2f %.2f\n", normala.x(), normala.y(), normala.z());

		//svetelny luc L
		svetelny_luc.setX(0);
		svetelny_luc.setY(0);
		svetelny_luc.setZ(10);
		svetelny_luc = norma(svetelny_luc); //znormovany

		//printf("svetelny luc L %.2f %.2f %.2f\n", svetelny_luc.x(), svetelny_luc.y(), svetelny_luc.z());

		//odrazeny luc R
		//2 * L  * N * N - L 
		odrazeny_luc.setX(2 * (svetelny_luc.x() * normala.x()) * normala.x() - svetelny_luc.x());
		odrazeny_luc.setY(2 * (svetelny_luc.y() * normala.y()) * normala.y() - svetelny_luc.y());
		odrazeny_luc.setZ(2 * (svetelny_luc.z() * normala.z()) * normala.z() - svetelny_luc.z());
		odrazeny_luc = norma(odrazeny_luc);

	//	printf("odrazeny luc R %.2f %.2f %.2f\n", odrazeny_luc.x(), odrazeny_luc.y(), odrazeny_luc.z());

		//vektor pohladu V
		vektor_pohladu.setX(0);
		vektor_pohladu.setY(0);
		vektor_pohladu.setZ(20);
		vektor_pohladu = norma(vektor_pohladu);

	//	printf("vektor pohladu V %.2f %.2f %.2f\n", vektor_pohladu.x(), vektor_pohladu.y(), vektor_pohladu.z());

		//ambientna zlozka
		IAmbientna[j] = QColor (kontrola_farby(IOr * rar), kontrola_farby(IOg * rag), kontrola_farby(IOb * rab));
	//	printf("ambientna %d %d %d %d\n",j, IAmbientna[j].red(), IAmbientna[j].green(), IAmbientna[j].blue());

		//difuzna zlozka
		if (((svetelny_luc.x() * normala.x()) + (svetelny_luc.y() * normala.y()) + (svetelny_luc.z() * normala.z())) <= 0)
			IZrkadlova[j] = QColor(0, 0, 0);
		else 
			IDifuzna[j] = QColor(kontrola_farby(ILr * rdr * (svetelny_luc.x() * normala.x())), 
				kontrola_farby(ILg * rdg * (svetelny_luc.y() * normala.y())),
				kontrola_farby(ILb * rdb * (svetelny_luc.z() * normala.z())));

		//printf("difuzna %d %d %d\n", IDifuzna[j].red(), IDifuzna[j].green(), IDifuzna[j].blue());

		//zrkadlova zlozka 
		if (((vektor_pohladu.x() * odrazeny_luc.x()) + (vektor_pohladu.y() * odrazeny_luc.y()) + 
			   (vektor_pohladu.z() * odrazeny_luc.z())) <= 0)
			IZrkadlova[j] = QColor(0, 0, 0);
		else if (((svetelny_luc.x() * normala.x()) + (svetelny_luc.y() * normala.y()) + (svetelny_luc.z() * normala.z())) <= 0)
			IZrkadlova[j] = QColor(0, 0, 0);
		else 
			IZrkadlova[j] = QColor(kontrola_farby(ILr * rsr * pow((vektor_pohladu.x() * odrazeny_luc.x()), h)),
		 		kontrola_farby(ILg * rsg * pow((vektor_pohladu.y() * odrazeny_luc.y()), h)),
				kontrola_farby(ILb * rsb * pow((vektor_pohladu.y() * odrazeny_luc.y()), h)));

		//printf("zrkadlova %d %d %d\n", IZrkadlova[j].red(), IZrkadlova[j].green(), IZrkadlova[j].blue());

		//vysledna farba
		farba[j] = QColor(kontrola_farby(IAmbientna[j].red() + IDifuzna[j].red() + IZrkadlova[j].red()),
			kontrola_farby(IAmbientna[j].green() + IDifuzna[j].green() + IZrkadlova[j].green()),
			kontrola_farby(IAmbientna[j].blue() + IDifuzna[j].blue() + IZrkadlova[j].blue()));

	//	printf("farba %d %d %d\n", farba[j].red(), farba[j].green(), farba[j].blue());
	}
	
	return farba;
}

void PaintWidget::ZBuffer(QVector3D bod, QColor farba)
{
	QPainter painter(&image);
	painter.setPen(QPen(farba, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	painter.setBrush(QBrush(farba));

	
	if (Z[bod.y()][bod.x()] > bod.z())
	{
		Z[bod.y()][bod.x()] = bod.z();
	//	Farba[bod.y()][bod.x()] = farba;
		//painter.setPen(farba);
		painter.drawPoint(bod.toPoint());
	}
}

void PaintWidget::vypln(QColor C, QVector3D a, QVector3D b, QVector3D c)
{
	//alokovanie Z pola
	int xs = image.width();
	int ys = image.height();

	//vytvorenie dvojrozmerneho pola pre ukladanie z suradnici
	Z.resize(xs);
	//F.resize(xs);
	for (int i = 0; i < xs; i++)
	{
		Z[i].resize(ys);
		//F[i].resize(ys);
	}

	//naplnenie tychto poli
	for (int i = 0; i < xs; i++)
	{
		for (int j = 0; j < ys; j++)
		{
			Z[i][j] = (HUGE_VALF);
		}
	}

	//sortovanie bodov trojuholnika podla y
	if (a.y() > c.y())
	{
		QVector3D tmp = a;
		a = c;
		c = tmp;
	}

	if (a.y() > b.y())
	{
		QVector3D tmp = a;
		a = b;
		b = tmp;
	}

	if (b.y() > c.y())
	{
		QVector3D tmp = b;
		b = c;
		c = tmp;
	}
	//printf("X sort a %f b %f c %f\n", a.x(), b.x(), c.x());
	//printf("Y sort a %f b %f c %f\n", a.y(), b.y(), c.y());

	//ak sa nerovnaju 2 y-ove suradnice, vyrata sa bod D
	QVector3D D;
	if ((a.y() != b.y()) && (b.y() != c.y()))
	{
		D.setY(b.y());
		D.setX(a.x() + (D.y() - a.y())*((c.x() - a.x())) / (c.y() - a.y()));
		D.setZ(a.z() + (D.y() - a.y())*((c.z() - a.z())) / (c.y() - a.y()));
	//	printf("d %f %f %f \n", D.x(), D.y(), D.z());

		//horny trojuholnik
		//hrany AB,AD
		QVector<float>hrana1;
		hrana1.push_back(a.x());
		hrana1.push_back(a.y());
		hrana1.push_back((b.y() - 1));
		hrana1.push_back(((b.x() - a.x()) / (b.y() - a.y())));
		hrana1.push_back(b.x());

		QVector<float>hrana2;
		hrana2.push_back(a.x());
		hrana2.push_back(a.y());
		hrana2.push_back((D.y() - 1));
		hrana2.push_back(((D.x() - a.x()) / (D.y() - a.y())));
		hrana2.push_back(D.x());

		if (hrana1[4] > hrana2[4])
			std::swap(hrana1, hrana2);

	//	printf("horny\nth1\n %f %f %f %f %f\n", hrana1[0], hrana1[1], hrana1[2], hrana1[3], hrana1[4]);
	//	printf("th2\n %f %f %f %f %f\n", hrana2[0], hrana2[1], hrana2[2], hrana2[3], hrana2[4]);

		//skontrolujeme ci je v platne vykreslime bod A
		if (!kontrola_bodu(a))
			ZBuffer(a, C);

		int ymin = (int)a.y();
		int ymax = (int)b.y();
		float x1 = hrana1[0];	//kvoli priesecnikom
		float x2 = hrana2[0];	//kvoli priesecnikom
		int ya;

		for (int ya = ymin; ya < ymax; ya++)
		{
			x1 += hrana1[3];
			x2 += hrana2[3];

			//printf("x1 %f x2 %f \n", x1, x2);

			//	x1= tabulkaHran[0][3] * ya - tabulkaHran[0][3] * tabulkaHran[0][1] + tabulkaHran[0][0];
			//	x2 = tabulkaHran[1][3] * ya - tabulkaHran[1][3] * tabulkaHran[1][1] + tabulkaHran[1][0];

			//z suradnica z interpolacie
			float z1, z2;
			if (hrana1[1] == hrana2[2])
				z1 = hrana1[1];
			z1 = a.z() + (ya - hrana1[1])*((b.z() - a.z()) / (hrana1[2] - hrana1[1]));

			if (hrana2[1] == hrana2[2])
				z2 = hrana2[1];
			z2 = a.z() + (ya - hrana2[1])*((D.z() - a.z()) / (hrana2[2] - hrana2[1]));

			if (!kontrola_bodu(QVector3D(x1, ya, z1)))
				ZBuffer(QVector3D(x1, ya, z1), C);
			if (!kontrola_bodu(QVector3D(x2, ya, z2)))
				ZBuffer(QVector3D(x2, ya, z2), C);

			for (int i = 0; i < x2 - x1; i++)
			{
				if (x1 == x2)
					z2 = x1;
				float z3 = z1 + ((x1 + i) - x1)*((z2 - z1) / (x2 - x1));
				if (!kontrola_bodu(QVector3D(x1 + i, ya, z3)))
					ZBuffer(QVector3D(x1 + i, ya, z3), C);
			}
		}


		//dolny trojuholnik
		//hrany BC,DC
		hrana1.clear();
		hrana1.push_back(b.x());
		hrana1.push_back(b.y());
		hrana1.push_back((c.y() - 1));
		hrana1.push_back(((c.x() - b.x()) / (c.y() - b.y())));

		hrana2.clear();
		hrana2.push_back(D.x());
		hrana2.push_back(D.y());
		hrana2.push_back((c.y() - 1));
		hrana2.push_back(((c.x() - D.x()) / (c.y() - D.y())));

		if (hrana1[0] > hrana2[0])
			std::swap(hrana1, hrana2);

	//	printf("dolny\nth1\n %f %f %f %f \n", hrana1[0], hrana1[1], hrana1[2], hrana1[3]);
		//printf("th2\n %f %f %f %f \n", hrana2[0], hrana2[1], hrana2[2], hrana2[3]);

		//skontrolujeme ci su v platne vykreslime body B,D
		if (!kontrola_bodu(b))
			ZBuffer(b, C);
		if (!kontrola_bodu(D))
			ZBuffer(D, C);

		int ymin2 = (int)b.y();
		int ymax2 = (int)c.y();
		float x11 = hrana1[0];	//kvoli priesecnikom
		float x22 = hrana2[0];	//kvoli priesecnikom
		int yaa;

		for (int yaa = ymin2; yaa < ymax2; yaa++)
		{
			x11 += hrana1[3];
			x22 += hrana2[3];

			//printf("x1 %f x2 %f \n", x11, x22);

			//	x11 = tabulkaHran[0][3] * yaa - tabulkaHran[0][3] * tabulkaHran[0][1] + tabulkaHran[0][0];
			//	x22 = tabulkaHran[1][3] * yaa - tabulkaHran[1][3] * tabulkaHran[1][1] + tabulkaHran[1][0];

			//z suradnica z interpolacie
			float z1, z2;
			if (hrana1[1] == hrana1[2])
				z1 = hrana1[1];
			z1 = c.z() + (yaa - hrana1[1])*((a.z() - b.z()) / hrana1[2] - hrana1[1]);

			if (hrana2[1] == hrana2[2])
				z2 = hrana2[1];
			z2 = c.z() + (yaa - hrana2[1])*((a.z() - D.z()) / (hrana2[2] - hrana2[1]));

			if (!kontrola_bodu(QVector3D(x11, yaa, z1)))
				ZBuffer(QVector3D(x11, yaa, z1), C);
			if (!kontrola_bodu(QVector3D(x22, ya, z2)))
				ZBuffer(QVector3D(x22, yaa, z2), C);

			for (int i = 0; i < x22 - x11; i++)
			{
				if (x11 == x22)
					z2 = x11;
				float z3 = z1 + ((x11 + i) - x11)*((z2 - z1) / (x22 - x11));
				if (!kontrola_bodu(QVector3D(x11 + i, yaa, z3)))
					ZBuffer(QVector3D(x11 + i, yaa, z3), C);
			}

			if (!kontrola_bodu(c))
				ZBuffer(c, C);
		}

	}

	//ak sa nejake dva y rovnaju 
	else //if(((a.y()==b.y())&&(a.y()!=c.y())) || ((b.y()==c.y())&&(b.y()!=a.y())) || ((a.y()==c.y())&&(a.y()!=b.y())))
	{
		tabulkaHran.clear();

		if (a.y() > c.y())
		{
			QVector3D tmp = a;
			a = c;
			c = tmp;
		}

		if (a.y() > b.y())
		{
			QVector3D tmp = a;
			a = b;
			b = tmp;
		}

		if (b.y() > c.y())
		{
			QVector3D tmp = b;
			b = c;
			c = tmp;
		}

		printf("%.2f %.2f %.2f\n", a.y(), b.y(), c.y());

		if (b.y() == c.y())
		{
			QVector<float>hrana1;
			hrana1.push_back(a.x());
			hrana1.push_back(a.y());
			hrana1.push_back((b.y() - 1));
			hrana1.push_back(((b.x() - a.x()) / (b.y() - a.y())));
			hrana1.push_back(b.x());

			QVector<float>hrana2;
			hrana2.push_back(a.x());
			hrana2.push_back(a.y());
			hrana2.push_back((c.y() - 1));
			hrana2.push_back(((c.x() - a.x()) / (c.y() - a.y())));
			hrana2.push_back(c.x());

			if (hrana1[4] > hrana2[4])
				std::swap(hrana1, hrana2);

			//	printf("horny\nth1\n %f %f %f %f %f\n", hrana1[0], hrana1[1], hrana1[2], hrana1[3], hrana1[4]);
			//	printf("th2\n %f %f %f %f %f\n", hrana2[0], hrana2[1], hrana2[2], hrana2[3], hrana2[4]);

			//skontrolujeme ci je v platne vykreslime bod A
			if (!kontrola_bodu(a))
				ZBuffer(a, C);

			int ymin = (int)a.y();
			int ymax = (int)b.y();
			float x1 = hrana1[0];	//kvoli priesecnikom
			float x2 = hrana2[0];	//kvoli priesecnikom
			int ya;

			for (int ya = ymin; ya < ymax; ya++)
			{
				x1 += hrana1[3];
				x2 += hrana2[3];

				//printf("x1 %f x2 %f \n", x1, x2);

				//	x1= tabulkaHran[0][3] * ya - tabulkaHran[0][3] * tabulkaHran[0][1] + tabulkaHran[0][0];
				//	x2 = tabulkaHran[1][3] * ya - tabulkaHran[1][3] * tabulkaHran[1][1] + tabulkaHran[1][0];

				//z suradnica z interpolacie
				float z1, z2;
				if (hrana1[1] == hrana2[2])
					z1 = hrana1[1];
				z1 = a.z() + (ya - hrana1[1])*((b.z() - a.z()) / (hrana1[2] - hrana1[1]));

				if (hrana2[1] == hrana2[2])
					z2 = hrana2[1];
				z2 = a.z() + (ya - hrana2[1])*((D.z() - a.z()) / (hrana2[2] - hrana2[1]));

				if (!kontrola_bodu(QVector3D(x1, ya, z1)))
					ZBuffer(QVector3D(x1, ya, z1), C);
				if (!kontrola_bodu(QVector3D(x2, ya, z2)))
					ZBuffer(QVector3D(x2, ya, z2), C);

				for (int i = 0; i < x2 - x1; i++)
				{
					if (x1 == x2)
						z2 = x1;
					float z3 = z1 + ((x1 + i) - x1)*((z2 - z1) / (x2 - x1));
					if (!kontrola_bodu(QVector3D(x1 + i, ya, z3)))
						ZBuffer(QVector3D(x1 + i, ya, z3), C);
				}
			}
		}

		else if (b.y() == a.y())
		{
			QVector<float>hrana1;
			hrana1.clear();
			hrana1.push_back(a.x());
			hrana1.push_back(a.y());
			hrana1.push_back((c.y() - 1));
			hrana1.push_back(((c.x() - a.x()) / (c.y() - a.y())));

			QVector<float>hrana2;
			hrana2.clear();
			hrana2.push_back(b.x());
			hrana2.push_back(b.y());
			hrana2.push_back((c.y() - 1));
			hrana2.push_back(((c.x() - b.x()) / (c.y() - b.y())));

			if (hrana1[0] > hrana2[0])
				std::swap(hrana1, hrana2);

			//	printf("dolny\nth1\n %f %f %f %f \n", hrana1[0], hrana1[1], hrana1[2], hrana1[3]);
			//printf("th2\n %f %f %f %f \n", hrana2[0], hrana2[1], hrana2[2], hrana2[3]);

			//skontrolujeme ci su v platne vykreslime body A,B
			if (!kontrola_bodu(a))
				ZBuffer(a, C);
			if (!kontrola_bodu(b))
				ZBuffer(b, C);

			int ymin2 = (int)a.y();
			int ymax2 = (int)c.y();
			float x11 = hrana1[0];	//kvoli priesecnikom
			float x22 = hrana2[0];	//kvoli priesecnikom
			int yaa;

			for (int yaa = ymin2; yaa < ymax2; yaa++)
			{
				x11 += hrana1[3];
				x22 += hrana2[3];

				//printf("x1 %f x2 %f \n", x11, x22);

				//	x11 = tabulkaHran[0][3] * yaa - tabulkaHran[0][3] * tabulkaHran[0][1] + tabulkaHran[0][0];
				//	x22 = tabulkaHran[1][3] * yaa - tabulkaHran[1][3] * tabulkaHran[1][1] + tabulkaHran[1][0];

				//z suradnica z interpolacie
				float z1, z2;
				if (hrana1[1] == hrana1[2])
					z1 = hrana1[1];
				z1 = c.z() + (yaa - hrana1[1])*((c.z() - a.z()) / hrana1[2] - hrana1[1]);

				if (hrana2[1] == hrana2[2])
					z2 = hrana2[1];
				z2 = c.z() + (yaa - hrana2[1])*((c.z() - b.z()) / (hrana2[2] - hrana2[1]));

				if (!kontrola_bodu(QVector3D(x11, yaa, z1)))
					ZBuffer(QVector3D(x11, yaa, z1), C);
				if (!kontrola_bodu(QVector3D(x22, yaa, z2)))
					ZBuffer(QVector3D(x22, yaa, z2), C);

				for (int i = 0; i < x22 - x11; i++)
				{
					if (x11 == x22)
						z2 = x11;
					float z3 = z1 + ((x11 + i) - x11)*((z2 - z1) / (x22 - x11));
					if (!kontrola_bodu(QVector3D(x11 + i, yaa, z3)))
						ZBuffer(QVector3D(x11 + i, yaa, z3), C);
				}

				if (!kontrola_bodu(c))
					ZBuffer(c, C);
			}
		}
		

		/*
		//tabulka hran
		QVector<QVector3D>bodyPom;
		
		bodyPom.push_back(a); bodyPom.push_back(b); bodyPom.push_back(c);
		float w, dx, dy;
		float x1, x2, y1, y2, z1, z2;
		for (int i = 0; i < bodyPom.size(); i++)
		{
			if (i != (bodyPom.size() - 1))
			{
				x1 = bodyPom[i].x();
				y1 = bodyPom[i].y();
				z1 = bodyPom[i].z();
				x2 = bodyPom[i + 1].x();	
				y2 = bodyPom[i + 1].y();
				z2 = bodyPom[i + 1].z();
			}

			else
			{
				x1 = bodyPom[bodyPom.size() - 1].x();
				y1 = bodyPom[bodyPom.size() - 1].y();
				z1 = bodyPom[bodyPom.size() - 1].z();
				x2 = bodyPom[0].x();
				y2 = bodyPom[0].y();
				z2 = bodyPom[0].z();
			}

		QVector<float>row;
		if ((y2 - y1) != 0)
		{
			if (y1 > y2)
			{
				std::swap(x1, x2);
				std::swap(y1, y2);
				std::swap(z1, z2);
			}
			
			dy = y2 - y1;
			w = (x2 - x1) / dy;
			row.push_back(x1);
			row.push_back(y1);
			row.push_back(y2 - 1);
			row.push_back(w);
			row.push_back(x2);
			row.push_back(z1);
			row.push_back(z2);
			tabulkaHran.push_back(row);
		}
	}

		printf("th1\n %f %f %f %f \n", tabulkaHran[0][0], tabulkaHran[0][1], tabulkaHran[0][2], tabulkaHran[0][3]);
		printf("th2\n %f %f %f %f \n", tabulkaHran[1][0], tabulkaHran[1][1], tabulkaHran[1][2], tabulkaHran[1][3]);

		//sort
		//ak je trojuholnik dolu hlavou
		if (tabulkaHran[0][4] == tabulkaHran[1][4])
		{
		if(tabulkaHran[0][0] > tabulkaHran[1][0])
		std::swap(tabulkaHran[0], tabulkaHran[1]);
		}
		//ak je trojuholnik hore hlavou
		if (tabulkaHran[0][0] == tabulkaHran[1][0])
		{
		if (tabulkaHran[0][4] > tabulkaHran[1][4])
		std::swap(tabulkaHran[0], tabulkaHran[1]);
		}

		printf("th1\n %f %f %f %f \n", tabulkaHran[0][0], tabulkaHran[0][1], tabulkaHran[0][2], tabulkaHran[0][3]);
		printf("th2\n %f %f %f %f \n", tabulkaHran[1][0], tabulkaHran[1][1], tabulkaHran[1][2], tabulkaHran[1][3]);

		//skontrolujeme ci je v platne vykreslime bod A
		if (!kontrola_bodu(QVector3D(tabulkaHran[0][0], tabulkaHran[0][1], tabulkaHran[0][5])))
		ZBuffer(QVector3D(tabulkaHran[0][0], tabulkaHran[0][1], tabulkaHran[0][5]), C);

		int ymin = (int)tabulkaHran[0][1];
		int ymax = (int)tabulkaHran[0][2];
		float xa = tabulkaHran[0][0];	//kvoli priesecnikom
		float xb = tabulkaHran[1][0];	//kvoli priesecnikom
		int ya;

		for (int ya = ymin; ya < ymax; ya++)
		{
		xa += tabulkaHran[0][3];
		xb += tabulkaHran[1][3];

		//	xa = tabulkaHran[0][3] * ya - tabulkaHran[0][3] * tabulkaHran[0][1] + tabulkaHran[0][0];
		//	xb = tabulkaHran[1][3] * ya - tabulkaHran[1][3] * tabulkaHran[1][1] + tabulkaHran[1][0];
		printf("priesecniky %f %f \n", xa, xb);
		//z suradnica z interpolacie
		float z1, z2;
		if (tabulkaHran[0][1] == tabulkaHran[0][2])
		z1 = tabulkaHran[0][1];
		z1 = tabulkaHran[0][5] + (ya - tabulkaHran[0][1])*((tabulkaHran[0][6] - tabulkaHran[0][5] / (tabulkaHran[0][2] - tabulkaHran[0][1])));

		if (tabulkaHran[1][1] == tabulkaHran[1][2])
		z2 = tabulkaHran[0][1];
		z2 = tabulkaHran[0][5] + (ya - tabulkaHran[0][1])*((tabulkaHran[1][6] - tabulkaHran[0][5]) / (tabulkaHran[0][2] - tabulkaHran[0][1]));

		if (!kontrola_bodu(QVector3D(xa, ya, z1)))
		ZBuffer(QVector3D(xa, ya, z1), C);
		if (!kontrola_bodu(QVector3D(xb, ya, z2)))
		ZBuffer(QVector3D(xb, ya, z2), C);

		for (int i = 0; i < x2 - x1; i++)
		{
			if (x1 == x2)
			z2 = x1;
			float z3 = z1 + ((xa + i) - xa)*((z2 - z1) / (xb - xa));
			if (!kontrola_bodu(QVector3D(x1 + i, ya, z3)))
			ZBuffer(QVector3D(xa + i, ya, z3), C);
		}

		}

		tabulkaHran.clear();*/
	}

}


void PaintWidget::DDA(QVector<QPoint>b)
{

		QPainter painter(&image);
		QColor C(255, 0, 0);
		painter.setPen(QPen(C, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
		painter.setBrush(QBrush(C));

		double dx, dy, steps, xx, yy, x, y;
		int x1, x2, y1, y2, x0, y0;

		for (int i = 0; i < b.size() - 1; i++)
		{
			x1 = b[i].x();
			y1 = b[i].y();
			x2 = b[i + 1].x();
			y2 = b[i + 1].y();

			dx = x2 - x1;
			dy = y2 - y1;

			if (fabs(dx) > fabs(dy))//smernica od 0 do 1
				steps = fabs(dx);

			else steps = fabs(dy);

			xx = dx / steps; //jedno smernica a druhe = 1 
			yy = dy / steps;

			x = x1;
			y = y1;

			for (int i = 1; i <= steps; i++)//rozdiel 
			{
				painter.drawEllipse((int)x, (int)y, 2, 2);
				x += xx;
				y += yy;
			}
			painter.drawEllipse(x, y, 2, 2);
		}

		update();
}

void PaintWidget::DDA2(QPoint prvy, QPoint druhy, QColor C)
{
	//vykresli na obrazovku
	int x1, y1, x2, y2, dx, dy, krok;
	float zvacsene_x, zvacsene_y, x, y;

	QPainter painter(&image);
	painter.setPen(QPen(C, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));

	x1 = prvy.x();
	y1 = prvy.y();
	x2 = druhy.x();
	y2 = druhy.y();

	//vypocita rozdiel medzi bodmi
	dx = x2 - x1;
	dy = y2 - y1;

	//pre zaciatok pripadi zaciatocny bod
	x = x1;
	y = y1;

	//rozhodne sa v ktorom smere sa bude krokuvat
	if (abs(dx) > abs(dy)) {
		krok = abs(dx);
	}
	else {
		krok = abs(dy);
	}

	//o kolko sa budu suradnice zvacsovat
	// if dx > dy -> zvacsenie_x = 1
	// if dx < dy -> zvacsenie_y = 1
	zvacsene_x = dx / (float)krok;
	zvacsene_y = dy / (float)krok;

	//zvacsovanie a vykreslovanie "ciare"
	for (int j = 0; j < krok; j++) {
		x += zvacsene_x;
		y += zvacsene_y;
		painter.drawPoint(x, y);
	}

	update();
}

void PaintWidget::otocenie_vlavo(int uhol)
{
	double u = M_PI / double(uhol);
	
	for (int i = 0; i < body.size(); i++)
	{
		double dlzkax = (double)body[i].x() - (double)body[0].x();
		double dlzkay = (double)body[i].y() - (double)body[0].y();

		body.replace(i, QPoint((int)round((dlzkax * cos(u)) + dlzkay * sin(u)) + body[0].x(),
			body[0].y() + (int)round((-dlzkax * sin(u)) + dlzkay * cos(u))));
	}
		
	update();
}

void PaintWidget::skalovanie(float koef)
{
	QPainter painter(&image);
	
	for (int i = 0; i < body.size(); i++)
	{
		body.replace(i, QPoint(body[0].x() + (int)round(((double)body[i].x() - (double)body[0].x()) * koef),
			body[0].y() + (int)round(((double)body[i].y() - (double)body[0].y()) * koef)));
	}

	update();
}

void PaintWidget::otocenie_vpravo(int uhol)
{
	double u = M_PI / double(uhol);

	for (int i = 0; i < body.size(); i++)
	{
		double dlzkax = (double)body[i].x() - (double)body[0].x();
		double dlzkay = (double)body[i].y() - (double)body[0].y();

		body.replace(i, QPoint((int)round((dlzkax * cos(u)) - dlzkay * sin(u)) + body[0].x(),
			body[0].y() + (int)round((dlzkax * sin(u)) + dlzkay * cos(u))));
	}

	update();
}

void PaintWidget::posunutie()
{
	clearImage();
	body_pom.clear();
	Bodiky.clear();

	QPoint Z, K;
	Z = posunutie_body[0];
	K = posunutie_body[posunutie_body.size() - 1];

	int posunX = K.x() - Z.x();
	int posunY = K.y() - Z.y();
	for (int i = 0; i < body.size(); i++)
	{
		int noveX, noveY;
		noveX = body[i].x() + posunX;
		noveY = body[i].y() + posunY;
		body_pom.push_back(QPoint(noveX, noveY));
	}
	
	for (int i = 0; i < indexy.size(); i++)
	{
		int it = indexy[i];
		Bodiky.push_back(body_pom[it]);
	}
	DDA(Bodiky);
	
	body.clear();
	body = body_pom;
	posunutie_body.clear();
}

void PaintWidget::plocha(int pocet_deleni, int pocet_kriviek)//rovnobezky poludniky
{
	
	fstream file;

	file.open("plocha1.vtk", fstream::out);

	file << "# vtk DataFile Version 3.0" << endl;
	file << "vtk output" << endl << "ASCII" << endl << "DATASET POLYDATA" << endl;
	
	QVector<QVector3D>KrivkoveBody;
	
	
	int delta = pocet_deleni;
	int pocet_bodov = BodyPlocha.size();
	float t = 0;
	QPointF B1 = QPointF(BodyPlocha[0].toPointF());

	KrivkoveBody.push_back(QVector3D(B1.x(), B1.y(), 0));

	QVector<QPointF> P1, P2;

	if (pocet_bodov < 2)
		return;

	P1.resize(pocet_bodov);
	P2.resize(pocet_bodov);

	QVector<QPoint> pomocny;

	for (int i = 0; i < pocet_bodov; i++) //priradenie povodnych bodov do vektora
		P1[i] = (QPointF)BodyPlocha[i].toPointF();

	while (t < 1)
	{
		t = t + 1.0 / delta;

		for (int j = 1; j < pocet_bodov; j++)
		{
			for (int i = 0; i < pocet_bodov - j; i++)
				P2[i] = (1 - t) * P1[i] + t *  P1[i + 1];

			P1 = P2;
		}

		pomocny.push_back(B1.toPoint());
		pomocny.push_back(P1[0].toPoint());
		
		B1 = P1[0];
		
		KrivkoveBody.push_back(QVector3D(B1.x(), B1.y(), 0)); //priradenie bodov na krivke do vektora
				
		for (int i = 0; i < pocet_bodov; i++)
			P1[i] = (QPointF)BodyPlocha[i].toPointF();

	}

	DDA(pomocny);
	pomocny.clear();

	BodyPlocha.clear();

	int size = KrivkoveBody.size();

	for (int i = 1; i < pocet_kriviek; i++)
	{
		for (int j = 0; j < size; j++)
		{
			QVector3D pom;

			pom.setX(KrivkoveBody[j].x());
			pom.setY(KrivkoveBody[j].y());
			pom.setZ(KrivkoveBody[j].z() + i*15);
			
			KrivkoveBody.push_back(pom);
		}
	}

	file << "POINTS " << (pocet_deleni + 1) * pocet_kriviek << " float" << endl;

	
	pocet_poly = 2 * pocet_deleni * (pocet_kriviek - 1);

	file << "POLYGONS " << pocet_poly << " " << 4 * pocet_poly << endl;

	int zac = 0;
	for (int i = 0; i < pocet_kriviek - 1; i++)
	{
		for (int j = 0; j < pocet_deleni; j++)
		{
			file << "3 " << zac + j << " " << zac + j + 1 << " " << zac + j + 1 + pocet_deleni << endl;
			indexy.push_back(zac + j);
			indexy.push_back(zac + j + 1);
			indexy.push_back(zac + j + 1 + pocet_deleni);
			indexy.push_back(zac + j);

			file << "3 " << zac + j + 1 << " " << zac + j + 2 + pocet_deleni << " " << zac + j + 1 + pocet_deleni << endl;
			indexy.push_back(zac + j + 1);
			indexy.push_back(zac + j + 2 + pocet_deleni);
			indexy.push_back(zac + j + 1 + pocet_deleni);
			indexy.push_back(zac + j + 1);
		}
		zac = zac + pocet_deleni + 1;
	}

	for (int i = 0; i < KrivkoveBody.size(); i++)
	{
		//zetove suradnice bodov
		surZ.push_back(KrivkoveBody[i].z()); 
		
		//suradnice x y
		body.push_back(QPoint(KrivkoveBody[i].toPoint()));
	}

	Zpom = surZ;

	file.close();
}