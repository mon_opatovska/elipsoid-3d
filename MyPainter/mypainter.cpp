#include "mypainter.h"

MyPainter::MyPainter(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	ui.scrollArea->setWidget(&this->paintWidget);
	ui.scrollArea->setBackgroundRole(QPalette::Dark);
}

MyPainter::~MyPainter()
{

}

void MyPainter::ActionOpen()
{
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "", "(*.vtk)");
	if (!fileName.isEmpty())
		paintWidget.openImage(fileName);

}

void MyPainter::ActionSave()
{
	QString fileName = QFileDialog::getSaveFileName(this, tr("Save As"), "untitled.png", tr("png Files (*.png)"));
	if (fileName.isEmpty()) {
		return;
	}
	else {
		paintWidget.saveImage(fileName);
	}
}

void MyPainter::EffectClear()
{
	paintWidget.clearImage();
	
	paintWidget.Bodiky.clear();
	paintWidget.body.clear();
	paintWidget.surZ.clear();
	paintWidget.indexy.clear();
}

void MyPainter::ActionNew()
{
	paintWidget.newImage(800, 600);
}

void MyPainter::Generuj()//elipsoid
{
	paintWidget.newImage(800, 600);
	int poludniky, rovnobezky;
	poludniky = ui.spinBox->value();
	rovnobezky = ui.spinBox_2->value();
	paintWidget.dz = ui.spinBox_3->value();

	paintWidget.generuj(rovnobezky, poludniky);

	//P.O.M.
	//farba svetla zo zdroja - dopadajuci luc
	paintWidget.ILr = ui.spinBox_4->value();
	paintWidget.ILg = ui.spinBox_5->value();
	paintWidget.ILb = ui.spinBox_6->value();
	//farba okoliteho svetla
	paintWidget.IOr = ui.spinBox_7->value();
	paintWidget.IOg = ui.spinBox_8->value();
	paintWidget.IOb = ui.spinBox_9->value();

	//koeficient odrazu
	paintWidget.rsr = ui.doubleSpinBox->value();
	paintWidget.rsg = ui.doubleSpinBox_2->value();
	paintWidget.rsb = ui.doubleSpinBox_3->value();

	//ostrost
	paintWidget.h = ui.spinBox_10->value();

	//koeficient difuzie
	paintWidget.rdr = ui.doubleSpinBox_4->value();
	paintWidget.rdg = ui.doubleSpinBox_5->value();
	paintWidget.rdb = ui.doubleSpinBox_6->value();

	//ambientny koeficient
	paintWidget.rar = ui.doubleSpinBox_7->value();
	paintWidget.rag = ui.doubleSpinBox_8->value();
	paintWidget.rab = ui.doubleSpinBox_9->value();
	
	//rovnobezne premietanie
	if (ui.radioButton_3->isChecked())
	{
		paintWidget.Bodiky.clear();

		QVector3D pom;
		
		//priradenie vrcholov do 3D vektora
		for (int i = 0; i < paintWidget.indexy.size(); i++)
		{
			paintWidget.Bodiky.push_back(paintWidget.body[paintWidget.indexy[i]]);

			pom.setX(paintWidget.Bodiky[i].x());
			pom.setY(paintWidget.Bodiky[i].y());
			pom.setZ(paintWidget.surZ[paintWidget.indexy[i]]);

			paintWidget.vrcholy.push_back(pom);
		}
		printf("vrcholy %d\n", paintWidget.vrcholy.size());
		
		//nastavenie farby z P.O.M
		QVector<QColor> F = paintWidget.Phongov_osv_model();
		
		
		//vykreslenie a vyfarbenie elipsoidu
		int j = 0;
		for (int i = 0; i < paintWidget.indexy.size() - 1; i++)
		{
			paintWidget.DDA2(paintWidget.Bodiky[i], paintWidget.Bodiky[i + 1], F[j]);

			//paintWidget.vypln(F[j], paintWidget.vrcholy[j], paintWidget.vrcholy[j + 1], paintWidget.vrcholy[j + 2]);

			if (i % 3 == 2)
				j++;
		}

		for (int i = 0; i < paintWidget.vrcholy.size(); i = i + 3)
		{
			printf("%d\n", i / 3);
			paintWidget.vypln(F[i], paintWidget.vrcholy[i], paintWidget.vrcholy[i + 1], paintWidget.vrcholy[i + 2]);
		
		}
	}

	
	//stredove premietanie
/*	if (ui.radioButton_4->isChecked())
	{
		int xs, ys, xx, yy;
		xs = paintWidget.image.width();
		ys = paintWidget.image.height();
		
		paintWidget.Bodiky.clear();
		
		for (int i = 0; i < paintWidget.indexy.size(); i++)
		{
			int it = paintWidget.indexy[i];
			xx = (paintWidget.body[it].x()) - (xs / 2);
			yy = (paintWidget.body[it].y()) - (ys / 2);

			paintWidget.Bodiky.push_back(QPoint((((xx*paintWidget.dz) / (paintWidget.dz - paintWidget.surZ[it])) + (xs / 2)),
				(((yy*paintWidget.dz) / (paintWidget.dz - paintWidget.surZ[it])) + (ys / 2))));

			paintWidget.surZ[i] = 0;
		}
		paintWidget.DDA(paintWidget.Bodiky);
	}*/
}

void MyPainter::Generuj2() //otvaranie zo suboru
{
	paintWidget.newImage(800, 600);
	//rovnobezne premietanie
	if (ui.radioButton_3->isChecked())
	{
		paintWidget.Bodiky.clear();
		
		for (int i = 0; i < paintWidget.indexy.size(); i++)
		{
			int it = paintWidget.indexy[i];
			paintWidget.Bodiky.push_back(paintWidget.body[it]);
		}
		paintWidget.DDA(paintWidget.Bodiky);
	}
}

void MyPainter::Plocha()
{
	int delenie, krivky;

	delenie = ui.spinBox_11->value();
	krivky = ui.spinBox_12->value();

	paintWidget.plocha(delenie, krivky);

	paintWidget.Bodiky.clear();

	for (int i = 0; i < paintWidget.indexy.size(); i++)
	{
		paintWidget.Bodiky.push_back(paintWidget.body[paintWidget.indexy[i]]);
	}
	
}

void MyPainter::Rotacia()
{
	paintWidget.clearImage();
	double theta, phi;
	theta = ui.horizontalSlider->value();
	phi = ui.horizontalSlider_2->value();
	
	if ((ui.radioButton_3->isChecked()) || ui.radioButton->isChecked())
		paintWidget.priemet = 0;

	if (ui.radioButton_4->isChecked())
		paintWidget.priemet = 1;

	paintWidget.rotuj(phi, theta, paintWidget.priemet);

	//priradenie vrcholov do 3D vektora
	
	paintWidget.vrcholy.clear();
	QVector3D pom;
	for (int i = 0; i < paintWidget.indexy.size(); i++)
	{
		pom.setX(paintWidget.Bodiky[i].x());
		pom.setY(paintWidget.Bodiky[i].y());
		pom.setZ(paintWidget.surZ[paintWidget.indexy[i]]);
		
		paintWidget.vrcholy.push_back(pom);
	}
	
	//farba z P.O.M
	QVector<QColor> F = paintWidget.Phongov_osv_model();

	//vykreslenie a vyfarbenie zrotovaneho elipsoidu
	int j = 0;
	for (int i = 0; i < paintWidget.indexy.size() - 1; i++)
	{
		paintWidget.DDA2(paintWidget.Bodiky[i], paintWidget.Bodiky[i + 1], F[j]);

		if (i % 3 == 2)
			j++;
	}

	for (int i = 0; i < paintWidget.vrcholy.size(); i = i + 3)
	{
	//	printf("%d\n", i / 3);
		paintWidget.vypln(F[i], paintWidget.vrcholy[i], paintWidget.vrcholy[i + 1], paintWidget.vrcholy[i + 2]);

	}
}

void MyPainter::Vykresli()
{
	paintWidget.clearImage();
	paintWidget.Bodiky.clear();
	paintWidget.koef = ui.lineEdit->text().toFloat();
	paintWidget.uhol= ui.lineEdit_2->text().toInt();
	
	if (ui.comboBox->currentIndex() == 0)
	{
		paintWidget.skalovanie(paintWidget.koef);
		
		for (int i = 0; i < paintWidget.indexy.size(); i++)
		{
			int it = paintWidget.indexy[i];
			paintWidget.Bodiky.push_back(paintWidget.body[it]);
		}
		paintWidget.DDA(paintWidget.Bodiky);
	}

	if (ui.comboBox->currentIndex() == 1)
	{
		paintWidget.otocenie_vlavo(paintWidget.uhol);
	
		for (int i = 0; i < paintWidget.indexy.size(); i++)
		{
			int it = paintWidget.indexy[i];
			paintWidget.Bodiky.push_back(paintWidget.body[it]);
		}
		paintWidget.DDA(paintWidget.Bodiky);
	}

	if (ui.comboBox->currentIndex() == 2)
	{
		paintWidget.otocenie_vpravo(paintWidget.uhol);
	
		for (int i = 0; i < paintWidget.indexy.size(); i++)
		{
			int it = paintWidget.indexy[i];
			paintWidget.Bodiky.push_back(paintWidget.body[it]);
		}
		paintWidget.DDA(paintWidget.Bodiky);
	}
}
