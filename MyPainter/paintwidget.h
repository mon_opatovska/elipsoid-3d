#ifndef PAINTWIDGET_H
#define PAINTWIDGET_H

#include <QColor>
#include <QImage>
#include <QPoint>
#include <QWidget>
#include <QtWidgets>
#include <QMessageBox>
#include <random>
#include <algorithm>
#include <iostream>
#include <fstream>


class PaintWidget : public QWidget
{
	Q_OBJECT

public:
	PaintWidget(QWidget *parent = 0);

	bool openImage(const QString &fileName);
	bool newImage(int x, int y);
	bool saveImage(const QString &fileName);
	void setPenColor(const QColor &newColor);
	void setPenWidth(int newWidth);
	void generuj(int rov, int pol);
	void DDA(QVector<QPoint>b);
	void DDA2(QPoint prvy, QPoint druhy, QColor C);
	void rotuj(double PHI, double THETA, bool priemet);
	QVector<QColor>Phongov_osv_model();
	void vypln(QColor C, QVector3D a, QVector3D b, QVector3D c);
	

	void otocenie_vlavo(int uhol);
	void otocenie_vpravo(int uhol);
	void skalovanie(float koef);
	void posunutie();
	void plocha(int pocet_deleni, int pocet_kriviek);
	bool kontrola_bodu(QVector3D bod);
	QVector3D norma(QVector3D vektor);
	int kontrola_farby(float zlozka);
	void ZBuffer(QVector3D bod, QColor farba);
	

	bool isModified() const { return modified; }
	QColor penColor() const { return myPenColor; }
	int penWidth() const { return myPenWidth; }
	bool moznost;
	QVector<QPoint>Bodiky;
	QVector<QVector3D>BodyPlocha;
	QVector<QPoint>body;
	QVector<QVector3D> vrcholy;
	QVector<QPoint>bodicky;
	QVector<int>indexy;
	QVector<int>surZ;
	QVector<int>Zpom;
	QVector<int>Zpom1;
	QVector<QVector<QColor>>Farba;
	QVector<QVector<float>>Z;
	QVector<QPoint>stredne;
	QVector< QVector<float>>tabulkaHran;
	QVector<QPoint>ABD;
	QVector<QPoint>ACD;
	QVector<int>ABDz;
	QVector<int>ACDz;
	QVector<QPoint>D;
	QVector<QColor>I;
	QImage image;
	int dz;
	int ILr, ILg, ILb;
	float rsg,rsr,rsb, h, rar, rag, rab, rdr, rdg, rdb;
	int IOr, IOg, IOb;
	//transformacie
	float koef;
	int uhol;
	QVector<QPoint>body_pom;
	bool rightclick = false;
	QVector<QPoint>posunutie_body;
	QPoint lastPoint2;
	bool priemet;
	int pocet_poly;

	public slots:
	void clearImage();

protected:
	void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void mouseMoveEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void mouseReleaseEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;
	void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE;
	void mouseDoubleClickEvent(QMouseEvent *event);
	

private:
	void drawLineTo(const QPoint &endPoint);
	void resizeImage(QImage *image, const QSize &newSize);
	int selectKth(int* data, int s, int e, int k);

	bool modified;
	bool painting;
	int myPenWidth;
	QColor myPenColor;
	QPoint lastPoint;
	
	int pom2;

	typedef struct SUR
	{
		double x;
		double y;
		double z;
	}SUR;
};

#endif // PAINTWIDGET_H